# Kubernetes

Provision Debian machines with Kubernetes toolsuite.

## Requirements

- `docker-ce`

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

```yml
kubernetes_packages:
  - name: kubelet
    state: present
  - name: kubeadm
    state: present
  - name: kubernetes-cni
    state: present
```

Kubernetes packages to be installed on the server. You can either provide a list of package names, or set `name` and `state` to have more control over whether the package is `present`, `absent`, `latest`, etc.

```yml
kubernetes_role: master
```

Whether the particular server will serve as a Kubernetes `master` (default) or `node`. The master will have `kubeadm init` run on it to intialize the entire K8s control plane, while `node`s will have `kubeadm join` run on them to join them to the `master`.

```yml
kubernetes_kubelet_extra_args: ""
```

Extra args to pass to `kubelet` during startup. E.g. to allow `kubelet` to start up even if there is swap is enabled on your server, set this to: `"--fail-swap-on=false"`.

```yml
    kubernetes_allow_pods_on_master: True
```

Whether to remove the taint that denies pods from being deployed to the Kubernetes master. If you have a single-node cluster, this should definitely be `True`. Otherwise, set to `False` if you want a dedicated Kubernetes master which doesn't run any other pods.

```yml
kubernetes_enable_web_ui: False
```

Whether to enable the Kubernetes web dashboard UI (only accessible on the master itself, or proxied).

```yml
kuberenetes_debug: False
```

Whether to show extra debug info in Ansible's logs (e.g. the output of the `kubeadm init` command).

```yml
kubernetes_pod_network_cidr: '10.0.1.0/16'
kubernetes_apiserver_advertise_address: ''
kubernetes_version: 'stable-1.10'
kubernetes_ignore_preflight_errors: 'all'
```

Options passed to `kubeadm init` when initializing the Kubernetes master. The `apiserver_advertise_address` defaults to `ansible_default_ipv4.address` if it's left empty.

```yml
kubernetes_apt_release_channel: main
kubernetes_apt_repository: "deb http://apt.kubernetes.io/ kubernetes-xenial {{ kubernetes_apt_release_channel }}"
kubernetes_apt_ignore_key_error: False
```

Apt repository options for Kubernetes installation.

```yml
kubernetes_yum_arch: x86_64
```

Yum repository options for Kubernetes installation.

```yml
kubernetes_flannel_manifest_file_rbac: https://raw.githubusercontent.com/coreos/flannel/master/Documentation/k8s-manifests/kube-flannel-rbac.yml
kubernetes_flannel_manifest_file: https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```

Flannel manifest files to apply to the Kubernetes cluster to enable networking. You can copy your own files to your server and apply them instead, if you need to customize the Flannel networking configuration.

## Dependencies


## Example Playbook

## Useful Kubernetes commands

```bash
kubectl --namespace=kube-system get pod
kubectl get service -o=yaml
kubectl get service --watch --namespace=kube-system
kubectl get service --watch --show-labels --namespace=kube-system

# if the install failed, reset everything before trying again
kubeadm reset

```

Another useful resource is the [Kubernetes cheatsheet][1]

## License
GPLv3

## Author Information
Daniel Andrei Minca <dminca@pm.me>

[1]: https://kubernetes.io/docs/reference/kubectl/cheatsheet/